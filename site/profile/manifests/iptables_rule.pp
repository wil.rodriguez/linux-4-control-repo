define profile::iptables_rule (
  $rule,
) {
  include ntp

  exec { "iptables_${name}":
    command => "iptables ${rule}",
    unless  => 'iptables -L -vt nat | grep 19999',
    path    => ["/bin/", "/usr/bin/", "/sbin"],
  }
}
