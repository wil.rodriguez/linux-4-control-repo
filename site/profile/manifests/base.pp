class profile::base (
  Boolean $debug_hiera = true,
) {

  include ntp

  package { 'tree':
      ensure    => installed,
  }

  package { 'bash-completion':
      ensure    => installed,
  }

  package { 'zsh':
      ensure =>   present,
  }

  package { 'iptables':
    ensure  => installed,
  }

  user { 'shaq':
    ensure     => present,
    uid        => 1337,
    gid        => 1337,
    home       => '/home/shaq',
    shell      => '/bin/zsh',
    managehome => true,
    require    => Package['zsh']
  }

  group { 'kazaam':
      ensure => present,
      gid    => 1337,
  }

  exec { 'install netdata':
    command => 'curl -Ss https://my-netdata.io/kickstart.sh | /bin/bash -s all --dont-wait --dont-start-it',
    path    => ['/bin/', '/usr/bin/'],
    unless  => 'test -d /etc/netdata',
  }

  service { 'netdata':
    ensure  => 'running',
    require => Exec['install netdata'],
  }

  profile::iptables_rule { 'netdata':
    rule    => '-t nat -I PREROUTING -p tcp --dport 8081 -j REDIRECT --to-ports 19999',
    require => Service['netdata'],
  }

  if $facts['pe_server_version'] {
    contain profile::base::bash_prompt
  }

  if $debug_hiera {
    notify { lookup('message'): }
  }

  file { '/root/blah.txt':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0640',
    source => 'puppet:///modules/profile/blah.txt',
  }
}
